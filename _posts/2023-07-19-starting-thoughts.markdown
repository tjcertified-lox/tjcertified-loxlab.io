Starting Thoughts
====

# Squandering time
It is so easy to squander time on new projects in the modern era. It used to be that the way to start code was:

1. open IDE or text editor
1. Start coding

Now (and this is better, I might add), the process for me was:

1. create repo
1. create CI pipeline
1. create blog project to collect my thoughts about the process
1. find an appropriate environment (since it is java, a simple text editor is just pain to use, so I went with intellij since I love JetBrains IDEs)
1. figure out a usable build system (ended up going with Kotlin-based Gradle for it)
1. fiddle with blog pipeline to publish on GitLab
1. fiddle with theming for blog engine
1. give up on fiddling with theming for blog engine.

All in all, this has taken about 3-4 weeks from the time I started reading the book for this project (the excellent [Crafting Interpreters by Robert Nystrom](https://craftinginterpreters.com/), available for free online, but also for purchase in eBook and dead-tree edition at your favorite retailers).
Finally, I decided to just go with the simple default Jekyll theming and move on.

Tools for this project:
1. Intellij (recommended IDE)
1. GitLab (recommended repo and CI/CD system)

So far, I am having fun. My attempt will be to write unit tests as I go along in the book as the book (correctly) does not waste time with them in print form, but leaves them as an exercise to the reader.

# Surprises
First, I was surprised that I was willing to spend as much time as I did trying to get a theme going for Jekyll. Additionally, I was surprised that I just could not get themes to work with all of the online documentation for the Jekyll system.
However, most of the docs focus on Git*hub* rather than Git**Lab**, so I guess I can forgive myself a bit.

I was also surprised that I am beginning to like the brace style of the book, OTBS. This is where the opening brace goes on the same line as the function declaration or the `if` statement. Usually, I use the Allman style, where braces are aligned. However, I really like the `else` portion when using OTBS. I lied a little here, because the style in the book is really K&R, which allows for no braces for a single-statement `if` or `while`, etc. I hate this, and will always provide braces due to being burned with missing braces in my coding career. It's a strong personal preference. Allman is a medium preference, and I have decided to let it go for this project in order to more closely match the style of the book.

I am just now in [Chapter 4 - Scanning](https://craftinginterpreters.com/scanning.html), and am about to add tests and reporting to GitLab for this project.

A final surprise is that I am using the Web Editor for GitLab, which I am enjoying. It shortens the cycle from writing to commit. I will likely only use this for _very_ short edits in code or pipeline files, and blog adds. I'm less concerned about quality in this blog.

Ok, one more surprise...I keep calling this blog a *wiki*, and it clearly is not that. If you are reading this and see mentions of a wiki, just know that I am referring to this **blog** actually, and forgive me.
